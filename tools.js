// Javascript Tool Collection
//
// Copyright (C) 2016  Benjamin Schnitzler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

"use strict";

// define a module for requirejs
define(function(){

var tools;

tools.privatize = function( variable, object, key )
{
  // function for accessing <variable>. if <key> is only known inside the
  // closure of the Class belonging to <object> (the private Namespace of the
  // Class), no one from outside can access <variable>.
  function access_private_variables( privacy_key )
  { return privacy_key === key ? variable : undefined }

  // object[real_propname] has only a getter an can thus not be overwritten, and
  // since it becomes a non-configurable property by default (see
  // defineProperty), this behaviour cannot be changed anylonger.
  Object.defineProperty( object, key.property_name, {
    get:function(){ return access_private_variables; } } );
}

tools.private_variable_accessor = function( property_name )
{
  function key(object){ return object[property_name](key) }
  key.property_name = property_name;
  return key;
}

tools.extend = function( Base, Child )
{
  function ctor(){}
  ctor.prototype = Base.prototype;
  Child.prototype = new ctor();
  Child.prototype.constructor = Child;
}

return tools;

});
